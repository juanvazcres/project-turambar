/*
app.controller('ImageController',function(ImageService,ChampionService){
    this.imgChamp;
    var champs=ChampionService.getChampions();
    this.getAllImgChamps = function(){

    };

    this.getImgChamp = function(key){

        this.imgChamp=ImgChampService.getImgChamp(key);
    };
});
*/

/*app.controller('SummonerController',function(SummonerServices,ChallengerServices,$scope){
    var vm=this;
    promesa = ChallengerServices.getSummoners().$promise.then(function(result) {
        vm.entries=result.entries;
        $scope.entries=result.entries;
    }, function(reason) {
        alert('Failed: ' + reason);
    });

    championsBySummonerId = SummonerServices.getChampionsBySummonerId();
});*/

app.controller('ChampionController',function(ChampionService,$state,$scope){
    this.champions=ChampionService.getChampions();
    //console.log(this.champions);
    this.sayhi=function(johncena){
        $scope.currentChamp=johncena;
        $state.go('statistics',{"key":johncena.key, id:johncena.id });
    }


    this.tiles = buildGridModel({
        icon : "avatar:svg-",
        title: "Svg-",
        background: ""
    });
    function buildGridModel(tileTmpl){
        var it, results = [ ];
        for (var j=0; j<11; j++) {
            it = angular.extend({},tileTmpl);
            it.icon  = it.icon + (j+1);
            it.title = it.title + (j+1);
            it.span  = { row : 1, col : 1 };
            switch(j+1) {
                case 1:
                    it.background = "red";
                    it.span.row = it.span.col = 2;
                    break;
                case 2: it.background = "green";         break;
                case 3: it.background = "darkBlue";      break;
                case 4:
                    it.background = "blue";
                    it.span.col = 2;
                    break;
                case 5:
                    it.background = "yellow";
                    it.span.row = it.span.col = 2;
                    break;
                case 6: it.background = "pink";          break;
                case 7: it.background = "darkBlue";      break;
                case 8: it.background = "purple";        break;
                case 9: it.background = "deepBlue";      break;
                case 10: it.background = "lightPurple";  break;
                case 11: it.background = "yellow";       break;
            }
            results.push(it);
        }
        return results;
    }
})
    .config( function( $mdIconProvider ){
    $mdIconProvider.iconSet("avatar", 'icons/avatar-icons.svg', 128);

});

app.controller('StatisticsController',function(champ,ChampionService,$state,$scope){
    var vm=this;
    vm.masterStats;
    vm.challengerStats;
    vm.key=champ.key;
    vm.id=champ.id;
    vm.tab=1;
    vm.dtInstance1  ={};
    vm.volver=function(){
        $state.go('home',{});
    };
    vm.setTab=function(tab){
        vm.tab=tab;
    };
    vm.isTab=function(tab){
        return vm.tab===tab;
    };
    vm.getRRS=function(summoner){
        var g=summoner.champStats.totalGoldEarned / summoner.champStats.totalSessionsPlayed;
        var k=summoner.champStats.totalChampionKills / summoner.champStats.totalSessionsPlayed;
        var d=summoner.champStats.totalDeathsPerSession / summoner.champStats.totalSessionsPlayed;
        var a=summoner.champStats.totalAssists / summoner.champStats.totalSessionsPlayed;
        var cs=summoner.champStats.totalMinionKills / summoner.champStats.totalSessionsPlayed;
        var kda=(k-d+a)/3;
        var t=summoner.champStats.totalSessionsPlayed;
        var rrs=g*(kda+cs)/100;
        return vm.round(rrs);
    }

    vm.round=function(value){
        return Math.round(value);
    };

    $scope.isLoading=true;

    vm.showLoader=function(flag){
        $scope.isLoading=flag;
    }

    challengerResultCallback = function(result){
        vm.showLoader(false);
        vm.challengerStats=result;
        console.log(vm.challengerStats);
        ChampionService.getChampionsDetailsByID(champ.id,
                                                "master",
                                                function(){vm.showLoader(true);},
                                                masterResultCallback,
                                                function(error){vm.showLoader(false);}
                                               );
    }

    masterResultCallback = function(result){
        vm.showLoader(false);
        vm.masterStats=result;
        console.log(vm.masterStats);
    }

    ChampionService.getChampionsDetailsByID(champ.id,
                                            "challenger",
                                            function(){vm.showLoader(true);},
                                            challengerResultCallback,
                                            function(error){
        vm.showLoader(false);
    }
                                           );



});
