var app = angular.module(
    'lolrr',
    ["ngMaterial",
     "ngResource",
     "datatables",
     "ui.router"])
.config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider.state('home', {
        url:'/home',
        views: {
            'contenedor': {
                templateUrl: 'templates/home.html',
                controller:'ChampionController as vm'

            }
        }

    })
        .state('statistics',{
        url:'/statistics/:id/:key',
        views: {
            'contenedor': {
                templateUrl: 'templates/ChampionPage.html',
                controller:'StatisticsController as vm'
            }
        },
        resolve:{
            champ: function($stateParams){
                return {key:$stateParams.key, id:$stateParams.id};
            }
        }
    });
    $urlRouterProvider.otherwise('/home');
});
