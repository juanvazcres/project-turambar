/*app.service('ImageService',function($resource){
    this.champIcon=$resource("http://ddragon.leagueoflegends.com/cdn/5.22.3/img/champion/:key.png",{key:'@key'});


    this.getImgChamp = function(key){
        return this.champIcon.get({key:key});
    };
this.splash=$resource("http://ddragon.leagueoflegends.com/cdn/img/champion/splash/:key.jpg",{key:'@key'});
    this.getSplashChamp = function(key){
        return this.splash.get({key:key});
};


});*/

app.service('ChampionService',function($resource,SummonerServices,$timeout){
    this.recursos = $resource("https://global.api.pvp.net/api/lol/static-data/lan/v1.2/champion?dataById=true&api_key=2fc8b931-27b7-4d3a-bac6-df2bdbf52196",{});
    this.getChampions=function(){
        return this.recursos.get();
    };

    /*this.volver=funtion(){
        $location.path("/home");
    };*/

    this.getChampionsDetailsByID=function(id,division,onBefore,onResult,onError){

        var profilerTimeout;
        var entries;
        var entriesIndex=0;
        var playerName;
        onBefore();
        SummonerServices.getSummoners(division).$promise.then(
            function(result){
                entries=result.entries;
                profilerTimeout=$timeout(getProfileCallback,300);
            },
            function(error){onError(error);});

        getProfileCallback = function(){
            currentSummoner=entries[entriesIndex];
            entriesIndex++;
            if(currentSummoner!=undefined){
                getSummonerProfile(currentSummoner)
                profilerTimeout=$timeout(getProfileCallback,300);
            }else{
                onResult(championStats);
                $timeout.cancel(profilerTimeout);
            }
        }
        var championStats = [];
        getSummonerProfile=function(player){
            SummonerServices.getChampionsBySummonerId(player.playerOrTeamId).$promise
                .then(function(result){
                console.log(player.playerOrTeamName);
                champ=filterChampByChampionID(id,result.champions);
                if(champ){
                    champ.stats.champID=champ.id;
                    player.champStats=champ.stats;
                    championStats.push(player);
                }


            },function(error){onError(error)});
        }


        filterChampByChampionID = function (idChampion , champions ){
            var champStats=false;
            angular.forEach(champions,function(champ){
                if(champ.id==idChampion){
                    champStats=champ;
                }
            });
            return champStats;
        }
    }
});

app.service('SummonerServices',function($resource,APIKEYService){
    this.recursos = $resource("https://lan.api.pvp.net/api/lol/lan/v1.3/stats/by-summoner/:summonerId/ranked?season=SEASON2015&api_key=:key",{summonerId:'summonerId',key:'key'});

    this.getChampionsBySummonerId = function(summonerId){
        return this.recursos.get({summonerId:summonerId,key:APIKEYService.getApiKey()});
    };

    this.recursosChallenger = $resource("https://lan.api.pvp.net/api/lol/lan/v2.5/league/:division?type=RANKED_SOLO_5x5&api_key=2fc8b931-27b7-4d3a-bac6-df2bdbf52196",{division:'division'});

    this.getSummoners=function(division){
        return this.recursosChallenger.get({division:division});
    };

});

app.service('APIKEYService', function(){

    var api_keys=['2fc8b931-27b7-4d3a-bac6-df2bdbf52196',
                  'd955bc8b-6b01-4356-9e69-0203cc65d9f4',
                  'f2c70014-f0ff-45c5-a97c-dfd4d8fd10a9',
                  'ecc8b224-190f-46d3-a8f8-873563a4d917',
                  '29b2a312-0ac0-4ca8-b064-63d519e8528a',
                  '25eb6412-8edb-4169-ad2e-668af363f0fe',
                  '12a00525-37c9-46fe-9b20-2e471365537d',
                  '9fe11c4f-19e7-40c2-904f-65dc4bd6de89',
                  '579251af-6d9d-4cfe-845e-2abfa67b2bba',
                  '0a4c65e6-a529-4e54-9060-51e5a9eef158',
                  '66cb5311-67f0-4059-b435-954ab5642264',
                  'da55f453-b3f6-42f5-9258-728ff4280401',
                  '5ed14fc1-d192-4b4e-a729-bd725a72cac3',
                  'b864edc3-3607-4f0a-8aff-37ac65115880',
                  '69da3d9d-be49-4a79-b82b-015b772ee058',
                  'b74f0553-1c09-4d91-adf6-11a98fa4a2cf',
                  '5145c6e4-c5eb-4483-8cbe-ae16ea9ef85b',
                  '347a7010-aa2b-4d8d-b7a3-c6fca961a725',
                  '02048bb8-4058-4439-8d5a-7666c61437fa',
                  '276d2544-02cb-4551-a0bb-4321403a90d7',
                  '2f7f3def-de1d-4e95-ba74-1e568574f5db',
                  'ed9cec45-65e7-456c-af54-eace1509d8ae',
                  '869285ca-69e2-4d0d-b990-2a71851cbf1d',
                  'fc824544-e573-4560-84fb-02f825e233f7',
                  '2e706abf-0116-4ed5-bb4c-d650c4b01ce2',
                  '7394aa52-aaf7-489a-aa9b-8a77db3b2480',
                  '21a1198f-93a6-4e85-af73-d38126a946a4',
                  '91607af3-2334-41f5-af93-60564576efc5',
                  '5573f714-c347-4568-895f-095fb1b9eba3',
                  '66f1c79a-034c-40b8-9e95-f67312fdd6ef',
                  'ba9a9962-a944-48fb-a9b4-e0a023c81404',
                  '81e01ea7-a69b-438b-a37b-d32e4a1b9bf6',
                  '4777367e-7f32-41dd-a540-c4348d2b4076',
                  'e165f8f0-8880-4f16-8b50-028e12c4634e',
                  '1c75f11e-a21c-41e6-98ff-8e309c96469a',
                  'f39fb994-46d1-48c0-a6e0-e29a8ae9aad7'
                  ];

    this.getApiKey=function(){
        index = Math.floor((Math.random() * api_keys.length));
        return api_keys[index];
    }

});
